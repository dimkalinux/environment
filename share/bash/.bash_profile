#! /usr/bin/env bash
#
# Load bash config files. These files have no functional difference other than
# to group similar configuration settings together for easy re-use.
#
# Exceptions:
#   ~/.bash_local can be used for settings you don’t want to commit

for file in bash_prompt bash_exports bash_aliases bash_auto bash_local bash_funcs; do
    file="$HOME/.$file"
    [ -e "$file" ] && source "$file"
done

# Load completions.
COMPLETION_FILES="$HOME/.completions/*"
for file in $COMPLETION_FILES
do
    source "$file"
done

# Load OS X specifics
if [ "$(uname)" == "Darwin" ]; then
    source $HOME/.bash_darwin
fi